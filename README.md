About the project:
```
A react project that uses youtube API. The user can search, select and play videos.
```
How to run: 
1. Clone the project
```
git clone git@bitbucket.org:sidney_pikot/youtubereact.git
```
2. Install dependencies
```
node version v12.8.1
npm install
```
3. Start project
```
npm start
```